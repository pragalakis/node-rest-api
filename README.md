# node-rest-api

A minimal Node.js REST API boilerplate,
built with Fastify and MongoDB that uses the [HAL](https://stateless.group/hal_specification.html) hypermedia type.

## How to run it?

- Install [Node.js](https://nodejs.org/)
- Install [Docker](https://www.docker.com/) or [Podman](https://podman.io/)
- Clone the repo
- Run `npm install`

### Start the services

1.  Build the api image: `docker build -t node-rest-api .`
2.  Run the database and the api: `docker-compose up`
3.  Access the api through `http://localhost:8080/health`

### Stop and clean up the services

1.  Stop the database and the api: `docker-compose down`
2.  Stop and remove the container:
    - `docker ps -a`
    - `docker stop <container_id>`
    - `docker rm <container_id>`
3.  Remove the image:
    - `docker images`
    - `docker rmi <image_id>`

## What's inside?

- `fastify`: a fast Node.js framework
- `mongoose`: Mongodb (nosql database) object modeling
- `jest`: a testing framework
- `uuid`: a package for creating unique ids
- `mongodb-memory-server`: a mongodb server for testing purposes

### Directory structure

```
.
├── docker-compose.yml
├── Dockerfile
├── .gitignore
├── LICENSE
├── package.json
├── package-lock.json
├── README.md
├── src
    ├── api
    │   ├── app.js
    │   ├── items
    │   │   └── itemsHandler.js
    │   ├── routes.js
    │   ├── schema
    │   │   └── routesSchemas.js
    │   └── validation
    │       └── requestParamsId.js
    ├── database
    │   ├── connectToDatabase.js
    │   ├── itemsModel.js
    │   └── __tests__
    │       └── database.test.js
    ├── index.js
    └── __tests__
        ├── deleteItem.test.js
        ├── getAllItems.test.js
        ├── getSingleItem.test.js
        ├── saveItem.test.js
        └── updateItem.test.js
```

### How it works

- `Dockerfile`: Builds the API docker image
- `docker-compose.yml`: Starts the database image (mongodb) and the API image
- `src/index.js`:
  - Starts the api
  - Starts the MongoDB database
  - Starts the server
- `src/api/app.js`: API orchestration.
  - Fastify's settings
  - Declares the API routes
- `src/database`:
  - Provides the database connection
  - Mongoose object model that provides database calls, schema definitions and validation
- `src/api/items/itemsHandler.js`: MongoDB create, read, update and delete operations and API responses
- `src/api/routes`: An array of all the routes where we define:
  - the method (`GET`, `POST`, `PUT`, `DELETE`)
  - the url of the api
  - schema (for request validation and response serialisation)
  - pre-validation hooks
  - the handlers (for the CRUD db operations)
- `src/api/schema/routesSchemas.js`: The schemas that are being used in the routes file
- `src/api/validation/requestParamsId.js`: A pre-validation function that is being used in the routes file
- `src/__tests__`: Integration API & database tests using fastify's `inject` and `mongodb-memory-server`

## CURL requests

### GET /health

```bash
curl -X "GET" http://localhost:8080/health
```

### GET a single item

```bash
curl -X GET http://localhost:8080/api/items/6158d9c1-31bb-4907-b3f0-25ee4a7b3d46
```

### GET all items

```bash
curl -X GET http://localhost:8080/api/items
```

### POST to create an item

```bash
curl -X POST -H "Content-Type: application/json" -d '{"name": "some-name", "timestamp": "2022-03-30T09:49:13.018Z"}' http://localhost:8080/api/items
```

### PUT to update an item

```bash
curl -X PUT -H "Content-Type: application/json" -d '{"name": "some-other-name", "timestamp": "2022-03-30T09:49:13.018Z"}' http://localhost:8080/api/items/6158d9c1-31bb-4907-b3f0-25ee4a7b3d46
```

### DELETE to remove an item

```bash
curl -X DELETE http://localhost:8080/api/items/6158d9c1-31bb-4907-b3f0-25ee4a7b3d46
```

## More things to do:

- Add link relations in `POST` and `PUT` and allow the api to accept only `json+hal` hypermedia type
- Integrate a swagger plugin and add documentation links in the api schema
- Add [fastify-helmet](https://www.npmjs.com/package/fastify-helmet)
- Load balancing
- Authentication & authorisation
- Cache
- Rate limiting
- Handling secrets and env configs properly
- Keep an eye on the [ajv-validation issue](https://github.com/ajv-validator/ajv-formats/issues/50) and deprecate the pre-validation hook for the params ids in favour of `ajv-format` UUIDs
- Add typescript
- CI/CD

## License

[MIT](./LICENSE)
