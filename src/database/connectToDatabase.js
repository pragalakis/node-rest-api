const mongoose = require('mongoose');

function startDatabase() {
  const uri = 'mongodb://root:example@mongo:27017/dbname?authSource=admin';
  const options = { family: 4 };

  mongoose.connect(
    uri,
    options
  );
  return mongoose.connection;
}

module.exports = startDatabase;
