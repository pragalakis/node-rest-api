const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const ItemsModel = require('../itemsModel');
const startDatabase = require('../connectToDatabase');
const uuid = require('uuid');

describe('Database Model', () => {
  let mongoServer;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    await mongoose.connect(
      mongoServer.getUri(),
      {}
    );
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  test('Does not save an item without an id', async () => {
    try {
      await new ItemsModel({ name: 'some-name' }).save();
    } catch (error) {
      expect(error.message).toBe('document must have an _id before saving');
    }

    expect.assertions(1);
    await mongoose.connection.dropDatabase();
  });

  test('Does not save a timestamp that is not a date', async () => {
    try {
      await new ItemsModel({ timestamp: 'Monday', _id: 'some-id' }).save();
    } catch (error) {
      expect(error.message).toBe(
        'items validation failed: timestamp: Cast to date failed for value "Monday" (type string) at path "timestamp"'
      );
    }

    expect.assertions(1);
    await mongoose.connection.dropDatabase();
  });

  test('Does not save an item with the same id', async () => {
    const id = uuid.v4();
    try {
      await new ItemsModel({ name: 'some-name', _id: id }).save();
      await new ItemsModel({ name: 'some-other-name', _id: id }).save();
    } catch (error) {
      const item = await ItemsModel.findById(id).lean();
      expect(item.name).toBe('some-name');

      expect(error.message).toBe(
        `E11000 duplicate key error collection: test.items index: _id_ dup key: { _id: "${id}" }`
      );
    }

    expect.assertions(2);
    await mongoose.connection.dropDatabase();
  });
});
