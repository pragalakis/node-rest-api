const mongoose = require('mongoose');

const itemsSchema = new mongoose.Schema({
  _id: String,
  name: String,
  timestamp: Date,
  category: String
});

module.exports = mongoose.model('items', itemsSchema);
