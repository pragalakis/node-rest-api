const startDatabase = require('./database/connectToDatabase');
const buildApp = require('./api/app');

const server = buildApp(true);

async function startServer() {
  try {
    await server.listen({ port: 8080, host: '0.0.0.0' });
    server.log.info(`server listening on ${server.server.address().port}`);
  } catch (error) {
    server.log.error(error);
    process.exit(1);
  }
}

startDatabase()
  .on('error', (error) => {
    server.log.error(error);
    process.exit(1);
  })
  .on('disconnected', startDatabase)
  .once('open', () => {
    server.log.info('MogndoDB connected');
    startServer();
  });
