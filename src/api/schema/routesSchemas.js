exports.healthCheckBodyResponse = {
  properties: {
    status: { type: 'string' }
  }
};

exports.itemsBodyResponse = {
  elements: {
    properties: {
      name: { type: 'string' },
      timestamp: { type: 'string' },
      category: { type: 'string' },
      _links: { self: { href: 'string' } }
    }
  }
};

exports.itemBodyResponse = {
  properties: {
    name: { type: 'string' },
    timestamp: { type: 'string' },
    category: { type: 'string' },
    _links: { self: { href: 'string' } }
  }
};

exports.linksBodyResponse = {
  properties: { _links: { self: { href: 'string' } } }
};

exports.contentTypeHeaders = {
  properties: {
    'Content-Type': { enum: ['application/json'] }
  }
};

exports.paramsRequest = {
  properties: { id: { type: 'string' } }
};

exports.bodyRequest = {
  properties: {
    name: { type: 'string' },
    timestamp: { type: 'timestamp' }
  },
  optionalProperties: { category: { type: 'string' } }
};

exports.errorSchema = {
  properties: {
    statusCode: { type: 'string' },
    error: { type: 'string' },
    message: { type: 'string' }
  }
};
