const itemsHandler = require('./items/itemsHandler');
const requestParamsIdValidation = require('./validation/requestParamsId');
const schema = require('./schema/routesSchemas');

const routes = [
  {
    method: 'GET',
    url: '/health',
    schema: {
      response: {
        200: schema.healthCheckBodyResponse,
        '5xx': schema.errorSchema
      }
    },
    handler: function(request, response) {
      response
        .code(200)
        .header('cache-control', 'no-cache')
        .send({ status: 'ok' });
    }
  },
  {
    method: 'GET',
    url: '/api/items',
    schema: {
      response: {
        200: schema.itemsBodyResponse,
        '4xx': schema.errorSchema,
        '5xx': schema.errorSchema
      }
    },
    handler: itemsHandler.getItems
  },
  {
    method: 'GET',
    url: '/api/items/:id',
    schema: {
      params: schema.paramsRequest,
      response: {
        200: schema.itemBodyResponse,
        '4xx': schema.errorSchema,
        '5xx': schema.errorSchema
      }
    },
    preValidation: requestParamsIdValidation,
    handler: itemsHandler.getSingleItem
  },
  {
    method: 'POST',
    url: '/api/items',
    schema: {
      headers: schema.contentTypeHeaders,
      body: schema.bodyRequest,
      response: {
        201: schema.linksBodyResponse,
        '4xx': schema.errorSchema,
        '5xx': schema.errorSchema
      }
    },
    handler: itemsHandler.addItem
  },
  {
    method: 'PUT',
    url: '/api/items/:id',
    schema: {
      params: schema.paramsRequest,
      headers: schema.contentTypeHeaders,
      body: schema.bodyRequest,
      response: {
        200: schema.linksBodyResponse,
        '4xx': schema.errorSchema,
        '5xx': schema.errorSchema
      }
    },
    preValidation: requestParamsIdValidation,
    handler: itemsHandler.updateItem
  },
  {
    method: 'DELETE',
    url: '/api/items/:id',
    schema: {
      params: schema.paramsRequest,
      response: {
        '4xx': schema.errorSchema,
        '5xx': schema.errorSchema
      }
    },
    preValidation: requestParamsIdValidation,
    handler: itemsHandler.deleteItem
  }
];

module.exports = routes;
