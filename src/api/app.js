const fastify = require('fastify');
const routes = require('./routes');

function buildApp(loggerStatus) {
  const app = fastify({
    logger: loggerStatus,
    jsonShorthand: false,
    ajv: { mode: 'JTD' }
  });

  // Routes
  routes.forEach((route) => {
    app.route(route);
  });

  return app;
}

module.exports = buildApp;
