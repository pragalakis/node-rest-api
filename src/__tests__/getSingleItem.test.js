const buildApp = require('../api/app');
const mongoose = require('mongoose');
const ItemsModel = require('../database/itemsModel');
const { MongoMemoryServer } = require('mongodb-memory-server');
const uuid = require('uuid');

describe('Gets a single item: /api/items/:id', () => {
  let server;
  let mongoServer;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    await mongoose.connect(
      mongoServer.getUri(),
      {}
    );

    server = buildApp();
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
    server.close();
  });

  test('Successfully gets a single item', async () => {
    // given
    const itemId = uuid.v4();
    const item = {
      name: 'some-name',
      timestamp: new Date().toISOString()
    };
    await new ItemsModel({ ...item, _id: itemId }).save();

    // when
    const response = await server.inject({
      method: 'GET',
      url: `/api/items/${itemId}`
    });

    // then
    expect(response.statusCode).toBe(200);
    expect(response.headers).toMatchObject({
      'content-type': 'application/hal+json; charset=utf-8'
    });
    expect(response.body).toBe(
      JSON.stringify({
        ...item,
        _links: { self: { href: `/api/items/${itemId}` } }
      })
    );

    await mongoose.connection.dropDatabase();
  });

  test('Returns "404 not found" if the id can not be found in the database', async () => {
    // given
    const id = uuid.v4();

    // when
    const response = await server.inject({
      method: 'GET',
      url: `/api/items/${id}`
    });

    //then
    expect(response.statusCode).toBe(404);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '404',
        error: 'Not Found',
        message: `The item with the id ${id} does not exist in the database.`
      })
    );
  });

  test('Failing parameter id validation sends an error ', async () => {
    // given
    const id = 'some-non-uuid';

    // when
    const response = await server.inject({
      method: 'GET',
      url: `/api/items/${id}`
    });

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'Request parameter id is not a UUID.'
      })
    );
  });

  test('Failing database sends an error', async () => {
    // given
    const id = uuid.v4();
    await mongoose.disconnect();

    // when
    const response = await server.inject({
      method: 'GET',
      url: `/api/items/${id}`
    });

    // then
    expect(response.statusCode).toBe(500);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '500',
        error: 'Internal Server Error',
        message: 'MongoClient must be connected to perform this operation'
      })
    );
  });
});
