const buildApp = require('../api/app');
const ItemsModel = require('../database/itemsModel');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const uuid = require('uuid');

describe('Update an item: /api/items/:id', () => {
  let server;
  let mongoServer;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    await mongoose.connect(
      mongoServer.getUri(),
      {}
    );
    server = buildApp();
  });

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
    server.close();
  });

  test('Successfully updates an item', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(200);
    expect(response.headers['content-type']).toBe(
      'application/hal+json; charset=utf-8'
    );
    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].name).toBe(requestData.name);
    expect(response.body).toEqual(
      JSON.stringify({
        _links: { self: { href: `/api/items/${id}` } }
      })
    );
  });

  test('Additional properties in the body request wont end up in the db', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      timestamp: new Date().toISOString(),
      some: 'additional-property'
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      category: 'some-existing-category',
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(200);
    expect(response.headers['content-type']).toBe(
      'application/hal+json; charset=utf-8'
    );
    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].name).toBe(requestData.name);
    expect(dbEntries[0]).toHaveProperty('name', requestData.name);
    expect(dbEntries[0]).toHaveProperty('timestamp');
    expect(dbEntries[0]).toHaveProperty('category', existingItem.category);
    expect(dbEntries[0]).toHaveProperty('_id');
    expect(dbEntries[0]).not.toHaveProperty('some', requestData.some);
  });

  test('Returns "404 not found" if the id can not be found in the database', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(404);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '404',
        error: 'Not Found',
        message: `The item with the id ${id} does not exist in the database.`
      })
    );

    expect(dbEntries.length).toEqual(0);
  });

  test('Failing parameter id validation sends an error', async () => {
    // given
    const id = 'some-non-uuid';
    const requestData = {
      name: 'some-other-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: uuid.v4()
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'Request parameter id is not a UUID.'
      })
    );
    expect(dbEntries[0].name).toBe(existingItem.name);
  });

  test('Incorrect content-type returns an error', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'text' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(415);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '415',
        error: 'Unsupported Media Type',
        message: 'Unsupported Media Type: text'
      })
    );

    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].name).toBe(existingItem.name);
  });

  test('Missing "name" from the body schema returns an error', async () => {
    // given
    const id = uuid.v4();
    const requestData = { timestamp: new Date().toISOString() };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: "body must have property 'name'"
      })
    );

    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].timestamp).toEqual(existingItem.timestamp);
  });

  test('Missing "timestamp" from the body schema returns an error', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      category: 'some-category'
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: "body must have property 'timestamp'"
      })
    );

    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].name).toBe(existingItem.name);
  });

  test('Invalid "name" type in the body schema returns an error', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: true,
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'body/name must be string'
      })
    );

    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].name).toBe(existingItem.name);
  });

  test('Invalid "timestamp" type in the body schema returns an error', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      timestamp: 'Monday',
      category: 'some-category'
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'body/timestamp must be timestamp'
      })
    );

    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].name).toBe(existingItem.name);
  });

  test('Invalid "category" type in the body schema returns an error', async () => {
    // given
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      timestamp: new Date().toISOString(),
      category: 1
    };
    const existingItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: id
    });
    await existingItem.save();

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'body/category must be string'
      })
    );

    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0].name).toBe(existingItem.name);
  });

  test('Failing database returns an error', async () => {
    // given
    await mongoose.disconnect();
    const id = uuid.v4();
    const requestData = {
      name: 'some-other-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };

    // when
    const response = await server.inject({
      method: 'PUT',
      url: `/api/items/${id}`,
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });

    // then
    expect(response.statusCode).toBe(500);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '500',
        error: 'Internal Server Error',
        message: 'MongoClient must be connected to perform this operation'
      })
    );
  });
});
