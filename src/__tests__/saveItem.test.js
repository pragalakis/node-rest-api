const buildApp = require('../api/app');
const ItemsModel = require('../database/itemsModel');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const uuid = require('uuid');

describe('Saves an item: /api/items', () => {
  let server;
  let mongoServer;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    await mongoose.connect(
      mongoServer.getUri(),
      {}
    );
    server = buildApp();
  });

  beforeEach(async () => {
    await mongoose.connection.dropDatabase();
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
    server.close();
  });

  test('Successfully saves an item', async () => {
    // given
    const requestData = {
      name: 'some-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(201);
    expect(response.headers['content-type']).toBe(
      'application/hal+json; charset=utf-8'
    );
    expect(dbEntries.length).toEqual(1);
    const id = dbEntries[0]['_id'];
    expect(uuid.validate(id)).toBe(true);
    expect(response.body).toEqual(
      JSON.stringify({
        _links: { self: { href: `/api/items/${id}` } }
      })
    );
  });

  test('Additional properties in the body request wont end up in the db', async () => {
    // given
    const requestData = {
      name: 'some-name',
      timestamp: new Date().toISOString(),
      category: 'some-category',
      some: 'additional-property'
    };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(201);
    expect(response.headers['content-type']).toBe(
      'application/hal+json; charset=utf-8'
    );
    expect(dbEntries.length).toEqual(1);
    expect(dbEntries[0]).toHaveProperty('name', requestData.name);
    expect(dbEntries[0]).toHaveProperty('timestamp');
    expect(dbEntries[0]).toHaveProperty('category', requestData.category);
    expect(dbEntries[0]).toHaveProperty('_id');
    expect(dbEntries[0]).not.toHaveProperty('some', requestData.some);
  });

  test('Incorrect content-type returns an error', async () => {
    // given
    const requestData = {
      name: 'some-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'text' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(415);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '415',
        error: 'Unsupported Media Type',
        message: 'Unsupported Media Type: text'
      })
    );
    expect(dbEntries.length).toEqual(0);
  });

  test('Missing "name" from the body schema returns an error', async () => {
    // given
    const requestData = {
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: "body must have property 'name'"
      })
    );
    expect(dbEntries.length).toEqual(0);
  });

  test('Missing "timestamp" from the body schema returns an error', async () => {
    // given
    const requestData = { name: 'some-name' };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: "body must have property 'timestamp'"
      })
    );
    expect(dbEntries.length).toEqual(0);
  });

  test('Invalid "name" type in the body schema returns an error', async () => {
    // given
    const requestdata = { name: 21321, timestamp: new Date().toISOString() };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestdata
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'body/name must be string'
      })
    );
    expect(dbEntries.length).toEqual(0);
  });

  test('Invalid "timestamp" type in the body schema returns an error', async () => {
    // given
    const requestData = {
      name: 'some-name',
      timestamp: 'some random string'
    };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'body/timestamp must be timestamp'
      })
    );
    expect(dbEntries.length).toEqual(0);
  });

  test('Invalid "category" type in the body schema returns an error', async () => {
    // given
    const requestData = {
      name: 'some-name',
      timestamp: new Date().toISOString(),
      category: true
    };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });
    const dbEntries = await ItemsModel.find().lean();

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'body/category must be string'
      })
    );
    expect(dbEntries.length).toEqual(0);
  });

  test('Failing database returns an error', async () => {
    // given
    await mongoose.disconnect();
    const requestData = {
      name: 'some-name',
      timestamp: new Date().toISOString()
    };

    // when
    const response = await server.inject({
      method: 'POST',
      url: '/api/items',
      headers: { 'Content-Type': 'application/json' },
      body: requestData
    });

    // then
    expect(response.statusCode).toBe(500);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '500',
        error: 'Internal Server Error',
        message: 'MongoClient must be connected to perform this operation'
      })
    );
  });
});
